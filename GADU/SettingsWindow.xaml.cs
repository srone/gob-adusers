﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GADU
{
    /// <summary>
    /// Logique d'interaction pour SettingsWindow.xaml
    /// </summary>
    public partial class SettingsWindow : Window
    {
        public SettingsWindow()
        {
            InitializeComponent();
        }

        private void SettingsWindow_Loaded(object sender, RoutedEventArgs e)
        {
            if (!File.Exists("GADU_language.txt"))
            {
                System.IO.File.WriteAllText("GADU_language.txt", "en-US");
            }
            else
            {
                string TxtLanguage = File.ReadAllText("GADU_language.txt");
                if (TxtLanguage.Contains("en-US"))
                {
                    ResourceDictionary dictionary = new ResourceDictionary();
                    dictionary.Source = new Uri("\\_system\\languages\\en-US.xaml", UriKind.Relative);
                    Resources.MergedDictionaries.Add(dictionary);
                }
                else if (TxtLanguage.Contains("fr-FR"))
                {
                    ResourceDictionary dictionary = new ResourceDictionary();
                    dictionary.Source = new Uri("\\_system\\languages\\fr-FR.xaml", UriKind.Relative);
                    Resources.MergedDictionaries.Add(dictionary);
                }
                else
                {
                    ResourceDictionary dictionary = new ResourceDictionary();
                    dictionary.Source = new Uri("\\_system\\languages\\en-US.xaml", UriKind.Relative);
                    Resources.MergedDictionaries.Add(dictionary);
                    System.IO.File.WriteAllText("GADU_language.txt", "en-US");
                }
            }
        }
    }
}
