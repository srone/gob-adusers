﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GADU
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            if (!File.Exists("GADU_language.txt"))
            {
                System.IO.File.WriteAllText("GADU_language.txt", "en-US");
                Francais_MenuItem.IsEnabled = true;
                Francais_MenuItem.IsChecked = false;
                English_MenuItem.IsEnabled = false;
                English_MenuItem.IsChecked = true;
            }
            else
            {
                string TxtLanguage = File.ReadAllText("GADU_language.txt");
                if (TxtLanguage.Contains("en-US"))
                {
                    Francais_MenuItem.IsEnabled = true;
                    Francais_MenuItem.IsChecked = false;
                    English_MenuItem.IsEnabled = false;
                    English_MenuItem.IsChecked = true;
                    ResourceDictionary dictionary = new ResourceDictionary();
                    dictionary.Source = new Uri("\\_system\\languages\\en-US.xaml", UriKind.Relative);
                    Resources.MergedDictionaries.Add(dictionary);
                }
                else if (TxtLanguage.Contains("fr-FR"))
                {
                    Francais_MenuItem.IsEnabled = false;
                    Francais_MenuItem.IsChecked = true;
                    English_MenuItem.IsEnabled = true;
                    English_MenuItem.IsChecked = false;
                    ResourceDictionary dictionary = new ResourceDictionary();
                    dictionary.Source = new Uri("\\_system\\languages\\fr-FR.xaml", UriKind.Relative);
                    Resources.MergedDictionaries.Add(dictionary);
                }
                else
                {
                    Francais_MenuItem.IsEnabled = true;
                    Francais_MenuItem.IsChecked = false;
                    English_MenuItem.IsEnabled = false;
                    English_MenuItem.IsChecked = true;
                    ResourceDictionary dictionary = new ResourceDictionary();
                    dictionary.Source = new Uri("\\_system\\languages\\en-US.xaml", UriKind.Relative);
                    Resources.MergedDictionaries.Add(dictionary);
                    System.IO.File.WriteAllText("GADU_language.txt", "en-US");
                }
            }
        }

        private void Settings_Click(object sender, RoutedEventArgs e)
        {
            var window = new SettingsWindow();

            window.Owner = this;
            window.Show();
        }
        private void English_Click(object sender, RoutedEventArgs e)
        {
            Francais_MenuItem.IsEnabled = true;
            Francais_MenuItem.IsChecked = false;
            English_MenuItem.IsEnabled = false;
            English_MenuItem.IsChecked = true;
            ResourceDictionary dictionary = new ResourceDictionary();
            dictionary.Source = new Uri("\\_system\\languages\\en-US.xaml", UriKind.Relative);
            Resources.MergedDictionaries.Add(dictionary);
            System.IO.File.WriteAllText("GADU_language.txt", "en-US");
        }
        private void Francais_Click(object sender, RoutedEventArgs e)
        {
            Francais_MenuItem.IsEnabled = false;
            Francais_MenuItem.IsChecked = true;
            English_MenuItem.IsEnabled = true;
            English_MenuItem.IsChecked = false;
            ResourceDictionary dictionary = new ResourceDictionary();
            dictionary.Source = new Uri("\\_system\\languages\\fr-FR.xaml", UriKind.Relative);
            Resources.MergedDictionaries.Add(dictionary);
            System.IO.File.WriteAllText("GADU_language.txt", "fr-FR");
        }
        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            txtOutput.Clear();
        }
    }
}
